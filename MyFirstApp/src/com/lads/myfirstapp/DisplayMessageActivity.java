package com.lads.myfirstapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class DisplayMessageActivity extends Activity { //Test

	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_display_message);
		// Show the Up button in the action bar.
		setupActionBar();
		
		Intent intent = getIntent();
		String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
		
		// Create the text view
	    TextView textView = new TextView(this);
	    textView.setTextSize(40);
	    textView.setText(message);

	    // Set the text view as the activity layout
	    //setContentView(textView);
		
	}

    
    @Override
    protected void onStart() {
        super.onStart();  // Always call the superclass method first
        
        // The activity is either being restarted or started for the first time
        System.out.println("Activity 2 Started");

    }
    
    @Override
    protected void onRestart() {
        super.onRestart();  // Always call the superclass method first
        System.out.println("Activity 2 Restarted");

        // Activity being restarted from stopped state    
    }
    
    @Override
    protected void onStop() {
        super.onStop();  // Always call the superclass method first

        System.out.println("Activity 2 Stopped");

    }
    
    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first
       System.out.println("Activity 2 Pause");
    }

    public void onResume() {
        super.onResume();  // Always call the superclass method first
        System.out.println("Activity 2 Resume");

        
    }
	
	
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.display_message, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	  public void callPhone(View view)
	  {
		Uri number = Uri.parse("tel:"+MainActivity.passed_message);
	    Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
	    startActivity(callIntent);
	  }
	  
	  public void openMap(View view)
	  {

		  Uri location = Uri.parse("geo:0,0?q=1600+Amphitheatre+Parkway,+Mountain+View,+California");
		  Intent mapIntent = new Intent(Intent.ACTION_VIEW, location);
		  startActivity(mapIntent);
	  }
	  public void openBrowser(View view) {
		  Uri webpage = Uri.parse("http://"+MainActivity.passed_message);
		  Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);
		  startActivity(webIntent);
	  }
	
}
